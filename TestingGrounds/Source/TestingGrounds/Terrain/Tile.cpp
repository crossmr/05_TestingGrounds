// Copyright Matthew Cross

#include "Tile.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/Actor.h"
#include "ActorPool1.h"
#include "NavigationSystem.h"
#include "Components/SkeletalMeshComponent.h"



// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	FVector NavigationBoundsOffset = FVector(2000, 0, 0);
	MinExtent = FVector(50, -1925, 0);
	MaxExtent = FVector(3950, 1925, 0);

}

template<class T>
void ATile::RandomlyPlaceActors(TSubclassOf<T> ToSpawn, FSpawnParameters SpawnParameters)
{
	int NumberToSpawn = FMath::RandRange(SpawnParameters.MinSpawn, SpawnParameters.MaxSpawn);
	for (size_t i = 0; i < NumberToSpawn; i++)
	{
		FSpawnPosition SpawnPosition;
		if (SpawnParameters.CanScale == true)
		{
			SpawnPosition.Scale = FMath::RandRange(SpawnParameters.MinScale, SpawnParameters.MaxScale);
		}
		else
		{
			SpawnPosition.Scale = SpawnParameters.MinScale;
		}
		bool found = FindEmptyLocation(SpawnPosition.Location, SpawnParameters.Radius * SpawnPosition.Scale);
		if (found)
		{
			//Go get a copy of this actor class
			//Set the location to the new spawn location
			//set visibility and collisions
			SpawnPosition.Rotation = FMath::RandRange(-180.f, 180.f);
			PlaceActor(ToSpawn, SpawnPosition, SpawnParameters.CanScale);
		}
		else
		{
			//UE_LOG(LogTemp, Warning, TEXT("Couldn't Find a spawn location"))
		}
	}
}

void ATile::PlaceAIPawns(TSubclassOf<APawn> ToSpawn, FSpawnParameters SpawnParameters)
{
	RandomlyPlaceActors(ToSpawn, SpawnParameters);
}

void ATile::PlaceActors(FSpawnParameters SpawnParameters)
{
	RandomlyPlaceActors(SpawnParameters.ToSpawn, SpawnParameters);
}

bool ATile::FindEmptyLocation(FVector& OutLocation, float Radius)
{
	FBox  Bounds(MinExtent, MaxExtent);
	const int MAX_ATTEMPTS = 25;
	for (size_t i = 0; i < MAX_ATTEMPTS; i++)
	{
		FVector CandidatePoint = FMath::RandPointInBox(FBox(Bounds));
		if (CanSpawnAtLocation(CandidatePoint, Radius))
		{
			OutLocation = CandidatePoint;
			return true;
		}	
	}
	return false;

}


void ATile::PlaceActor(TSubclassOf<APawn> ToSpawn, FSpawnPosition SpawnPosition, bool CanScale)
{
	APawn* Spawned = GetWorld()->SpawnActor<APawn>(ToSpawn, SpawnPosition.Location, FRotator(0, SpawnPosition.Rotation, 0));
	if (!Spawned) { return; }
	Spawned->SetActorRelativeLocation(SpawnPosition.Location);
	Spawned->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	Spawned->SpawnDefaultController();
	Spawned->Tags.Add(FName("Enemy"));
}

void ATile::PlaceActor(TSubclassOf<AActor> ToSpawn, FSpawnPosition SpawnPosition, bool CanScale)
{
	if (ActorPool == nullptr) 
	{
	UE_LOG(LogTemp, Warning, TEXT("Pool is Null")) 
	return; 
	}
	AActor* ActorToSet = ActorPool->SpawnCheckout(ToSpawn);
	ActorToSet->SetActorRelativeLocation(SpawnPosition.Location);
	ActorToSet->SetActorRelativeRotation(FRotator(0, SpawnPosition.Rotation, 0));
	ActorToSet->SetActorEnableCollision(true);
	ActorToSet->SetActorHiddenInGame(false);
	ActorToSet->SetActorTickEnabled(true);
	ActorToSet->Reset();
	ActorToSet->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	if (CanScale)
	{
		ActorToSet->SetActorScale3D(FVector(SpawnPosition.Scale));
	}
}	

bool ATile::CanSpawnAtLocation(FVector Location, float Radius)
{
	FHitResult HitResult;
	FVector GlobalLocation = ActorToWorld().TransformPosition(Location);
	bool HasHit = GetWorld()->SweepSingleByChannel(
	HitResult,
	GlobalLocation,
	GlobalLocation,
	FQuat::Identity,
	ECollisionChannel::ECC_GameTraceChannel2,
	//FCollisionShape::MakeSphere(Radius)
	FCollisionShape::MakeCapsule(Radius, 1000.f)
	);
	//FColor ResultColor = HasHit ? FColor::Red : FColor::Green;
	//DrawDebugSphere(GetWorld(), Location, Radius, 8, ResultColor, true, 100.f);
	return !HasHit;
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();	
	
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::SetPool(UActorPool1* InPool, UActorPool1* SpawnPool)
{
	Pool = InPool;
	ActorPool = SpawnPool;
	PositionNavMeshBoundsVolume();

}

void ATile::PositionNavMeshBoundsVolume()
{
	NavMeshBoundsVolume = Pool->Checkout();
	if (NavMeshBoundsVolume == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Not Enough NavMeshBoundsVolume Actors"));
		return;
	}
	NavMeshBoundsVolume->SetActorLocation(GetActorLocation()+NavigationBoundsOffset);
	UNavigationSystemV1::GetNavigationSystem(GetWorld())->Build();
}

void ATile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (NavMeshBoundsVolume == nullptr) 
	{ 
		UE_LOG(LogTemp, Warning, TEXT("%s has nothing to return"), *GetName());
		return;
	}
	Pool->Return(NavMeshBoundsVolume);	

	TArray<AActor*> ActorsToReturn;
	 GetAttachedActors(ActorsToReturn);
	 for (AActor* Actor : ActorsToReturn)
	 {
		 Actor->SetActorHiddenInGame(true);
		 Actor->SetActorEnableCollision(false);
		 Actor->SetActorTickEnabled(false);
		 ActorPool->SpawnReturn(Actor);
	 }

}



