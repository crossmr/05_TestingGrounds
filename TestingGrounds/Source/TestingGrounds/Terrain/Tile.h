// Copyright Matthew Cross

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

class UActorPool1;

USTRUCT()
struct FSpawnPosition
{
	GENERATED_USTRUCT_BODY()

	FVector Location;
	float Rotation;
	float Scale;
};


USTRUCT(BlueprintType)
struct FSpawnParameters
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<AActor> ToSpawn;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int MinSpawn;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int MaxSpawn;
	/**If Actor cannot scale, MinScale will be used as Scale value*/
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MinScale;
	/**If Actor cannot scale, MaxScale will be ignored*/
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MaxScale;
	/**This value will be multiplied by the scale size to determine collision radius*/
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float Radius;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	bool CanScale;

	float FActorSpawnInfoStruct()
	{
		MinSpawn = 1;
		MaxSpawn = 1;
		MinScale = 1;
		MaxScale = 1;
		Radius = 500;
		CanScale = false;
	}


};

UCLASS()
class TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void PlaceActors(FSpawnParameters SpawnParameters);

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void PlaceAIPawns(TSubclassOf<APawn> ToSpawn, FSpawnParameters SpawnParameters);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category = "Navigation")
		FVector NavigationBoundsOffset;

	UFUNCTION(BlueprintCallable, Category = "Pool")
		void SetPool(UActorPool1* InPool, UActorPool1* ActorPool);

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MinExtent;
	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MaxExtent;



private:
	bool CanSpawnAtLocation(FVector Location, float Radius);
	
	bool FindEmptyLocation(FVector& OutLocation, float Radius);

	template<class T>
	void RandomlyPlaceActors(TSubclassOf<T> ToSpawn, FSpawnParameters SpawnParameters);

	void PlaceActor(TSubclassOf<AActor> ToSpawn, FSpawnPosition SpawnPosition, bool CanScale);

	void PlaceActor(TSubclassOf<APawn> ToSpawn, FSpawnPosition SpawnPosition, bool CanScale);

	void PositionNavMeshBoundsVolume();

	UActorPool1* Pool;

	UActorPool1* ActorPool;

	AActor* NavMeshBoundsVolume;
	
};
