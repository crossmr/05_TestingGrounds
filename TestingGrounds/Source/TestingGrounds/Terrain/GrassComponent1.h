// Copyright Matthew Cross

#pragma once

#include "CoreMinimal.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "GrassComponent1.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))

class TESTINGGROUNDS_API UGrassComponent1 : public UHierarchicalInstancedStaticMeshComponent
{
	GENERATED_BODY()
public:
	// Sets default values for this component's properties
	UGrassComponent1();
	UPROPERTY(EditDefaultsOnly, Category = Spawning)
		FBox SpawningExtents;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Spawning)
		int SpawnCount;

	UFUNCTION(BluePrintCallable, Category = "Spawning")
	void SpawnGrass();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
private:
	

};