// Copyright Matthew Cross

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GuardController.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AGuardController : public AAIController
{
	GENERATED_BODY()

		AGuardController();

public:
	UFUNCTION(BlueprintCallable, Category = "Navigation")
	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;

	virtual void Tick(float DeltaTime) override;

	
	
};
