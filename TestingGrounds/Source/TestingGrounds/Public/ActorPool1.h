// Copyright Matthew Cross

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActorPool1.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTINGGROUNDS_API UActorPool1 : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	UActorPool1();

public:
	AActor * Checkout();
	void Return(AActor* ActorToReturn);
	void Add(AActor* ActorToAdd);

	AActor * SpawnCheckout(TSubclassOf<AActor> SpawnableActorToCheckout);
	void SpawnReturn(AActor* SpawnableActorToReturn);
	void SpawnAdd(AActor* SpawnableActorToAdd);

private:
	TArray<AActor*> Pool;

	TArray<AActor*> SpawnableActors;

	
};
