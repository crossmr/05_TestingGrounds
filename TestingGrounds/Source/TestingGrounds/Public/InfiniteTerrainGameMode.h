// Copyright Matthew Cross

#pragma once

#include "CoreMinimal.h"
#include "TestingGroundsGameMode.h"
#include "InfiniteTerrainGameMode.generated.h"



/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AInfiniteTerrainGameMode : public ATestingGroundsGameMode
{
	GENERATED_BODY()
	
public:
	AInfiniteTerrainGameMode();

	UFUNCTION(BlueprintCallable, Category = "BoundsPool")
		void PopulateBoundsVolumePool();

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void SeedActors(TSubclassOf<AActor> ToSeed, int NumberToSeed);


protected:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bounds Pool")
		class UActorPool1* NavMeshBoundsVolumePool;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bounds Pool")
		class UActorPool1* SpawnableActorsPool;

private:
	void AddToPool(class ANavMeshBoundsVolume *VolumeToAdd);

	void AddToSpawnPool(AActor *ActorToAdd);	
	
};
