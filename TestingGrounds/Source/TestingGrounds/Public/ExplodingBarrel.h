// Copyright Matthew Cross

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplodingBarrel.generated.h"


class URadialForceComponent;
class UParticleSystemComponent;

UCLASS()
class TESTINGGROUNDS_API AExplodingBarrel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplodingBarrel();

	//called by the engine when actor is damaged
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		URadialForceComponent* ExplosionForce = nullptr;

	UParticleSystem* ParticleFX1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class USoundBase* ExplosionSound;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float ProjectileDamage = 100.f;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* CollisionMesh = nullptr;

private:
	
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		int32 StartingHealth = 20;	

	UPROPERTY(VisibleAnywhere, Category = "Health")
		int32 CurrentHealth; //Initialized in begin play

	
};
