// Copyright Matthew Cross

#include "ActorPool1.h"
#include "UObject/UObjectBase.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"


// Sets default values
UActorPool1::UActorPool1()
{

}

AActor* UActorPool1::Checkout()
{

	
	if (Pool.Num() == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("No meshes in the pool"));
		return nullptr;
	}
	return Pool.Pop();
}

void UActorPool1::Return(AActor* ActorToReturn)
{
	Add(ActorToReturn);
}

void UActorPool1::Add(AActor* ActorToAdd)
{
	if (ActorToAdd == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("[%s] Added null actor."), *GetName());
		return;
	}
	Pool.Push(ActorToAdd);

}

AActor* UActorPool1::SpawnCheckout(TSubclassOf<AActor> SpawnableActorToCheckout)
{
	AActor* ActorToCheckout;	
	int32 Size = SpawnableActors.Num();
	//UE_LOG(LogTemp, Warning, TEXT("Spawnable Actors Length Checkout: %s"), *FString::FromInt(Size))
	if (Size > 0)
	{
		for (int32 Counter = 0; Counter < Size; Counter++)
		{
			if (SpawnableActors[Counter]->GetClass() == SpawnableActorToCheckout)
			{
				ActorToCheckout = SpawnableActors[Counter];
				SpawnableActors.RemoveAt(Counter);
				return ActorToCheckout;
			}
		}
	}

	UWorld* const World = GetWorld();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ActorToCheckout = World->SpawnActor<AActor>(SpawnableActorToCheckout, FVector(0), FRotator(0), SpawnParameters);
	ActorToCheckout->SetActorHiddenInGame(true);
	ActorToCheckout->SetActorEnableCollision(false);
	ActorToCheckout->SetActorTickEnabled(false);
	//UE_LOG(LogTemp, Warning, TEXT("I Had to Spawn an actor"))
	return ActorToCheckout;
}

void UActorPool1::SpawnReturn(AActor* SpawnableActorToReturn)
{
	SpawnAdd(SpawnableActorToReturn);
}

void UActorPool1::SpawnAdd(AActor* SpawnableActorToAdd)
{
	if (SpawnableActorToAdd == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("[%s] Added null actor."), *GetName());
		return;
	}
	SpawnableActors.Push(SpawnableActorToAdd);	
}