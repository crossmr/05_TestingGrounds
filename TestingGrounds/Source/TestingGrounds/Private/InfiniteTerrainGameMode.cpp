// Copyright Matthew Cross

#include "InfiniteTerrainGameMode.h"
#include "Navmesh/NavMeshBoundsVolume.h"
#include "GameFramework/Volume.h"
#include "EngineUtils.h"
#include "ActorPool1.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode()
{
	NavMeshBoundsVolumePool = CreateDefaultSubobject<UActorPool1>(FName("Nav Mesh Bounds Volume Pool"));
	SpawnableActorsPool = CreateDefaultSubobject<UActorPool1>(FName("Spawnable Actors Pool"));
}

void AInfiniteTerrainGameMode::PopulateBoundsVolumePool()
{
	auto VolumeIterator = TActorIterator<ANavMeshBoundsVolume>(GetWorld());
	while (VolumeIterator)
	{
		AddToPool(*VolumeIterator);
		++VolumeIterator;
	}
}

void AInfiniteTerrainGameMode::AddToPool(ANavMeshBoundsVolume *VolumeToAdd)
{
	//UE_LOG(LogTemp, Warning, TEXT("Found Actor %s"), *VolumeToAdd->GetName());
	NavMeshBoundsVolumePool->Add(VolumeToAdd);
}

void AInfiniteTerrainGameMode::SeedActors(TSubclassOf<AActor> ToSeed, int NumberToSeed)
{
	for (int32 Counter = 1; Counter <= NumberToSeed; Counter++)
	{
		UWorld* const World = GetWorld();
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AActor* ActorToSeed = World->SpawnActor<AActor>(ToSeed, FVector(0), FRotator(0), SpawnParameters);
		ActorToSeed->SetActorHiddenInGame(true);
		ActorToSeed->SetActorEnableCollision(false);
		ActorToSeed->SetActorTickEnabled(false);
		AddToSpawnPool(ActorToSeed);
	}
}

void AInfiniteTerrainGameMode::AddToSpawnPool(AActor *ActorToAdd)
{
	SpawnableActorsPool->SpawnAdd(ActorToAdd);
}


