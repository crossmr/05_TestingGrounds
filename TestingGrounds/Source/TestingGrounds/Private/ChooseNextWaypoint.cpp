// Fill out your copyright notice in the Description page of Project Settings.

#include "ChooseNextWaypoint.h"
#include "AIController.h"
#include "PatrolRootComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	//Get patrol points from Patrolling Guard
	auto ControlledPawn = OwnerComp.GetAIOwner()->GetPawn();
	auto PatrollingComponent = ControlledPawn->FindComponentByClass<UPatrolRootComponent>();
	if (!ensure(PatrollingComponent)) { return EBTNodeResult::Failed; } //make sure patrol route is assigned to pawn
	auto PatrolPoints = PatrollingComponent->GetPatrolPoints();
	if (PatrolPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("A Guard is missing Patrol Points"))
		return EBTNodeResult::Failed;
	}
	
	//Read the name of the waypoint and set it as the blackboard value
	auto BlackboardComp = OwnerComp.GetBlackboardComponent();
	auto Index = BlackboardComp->GetValueAsInt(IndexKey.SelectedKeyName);
	BlackboardComp->SetValueAsObject(WaypointKey.SelectedKeyName, PatrolPoints[Index]);


	//cycle through the array values
	auto NextIndex = (Index + 1) % PatrolPoints.Num();
	BlackboardComp->SetValueAsInt(IndexKey.SelectedKeyName, NextIndex);



	//UE_LOG(LogTemp, Warning, TEXT("Waypoint Index: %i"), Index)
	return EBTNodeResult::Succeeded;
}




